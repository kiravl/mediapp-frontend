import { Component, OnInit } from '@angular/core';
import { TOKEN_NAME } from './../../_shared/var.constant';
import * as decode from 'jwt-decode';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  user: Object;

  constructor() { }

  ngOnInit() {
    this.user = decode(JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token)
  }

}
