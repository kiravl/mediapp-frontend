import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import { Signos } from '../../_model/signos';
import { SignosService } from '../../_service/signos.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-signos',
  templateUrl: './signos.component.html',
  styleUrls: ['./signos.component.css']
})
export class SignosComponent implements OnInit {

  dataSource: MatTableDataSource<Signos>
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  displayedColumns = ['id', 'fecha', 'ritmo', 'pulso', 'paciente', 'acciones']
  mensaje: string;
  cantidad: number

  constructor(private signosService: SignosService, public route: ActivatedRoute, public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.signosService.signos.subscribe(data => {
      this.procesarDatos(data)
    })
    this.signosService.mensaje.subscribe(data => {
      this.snackBar.open(data, null, {
        duration: 2000,
      })
    })
    this.signosService.getLista(0, 100).subscribe(data => {
      this.procesarDatos(data)
    })
  }

  private procesarDatos(data: Signos[]) {
    let signos = JSON.parse(JSON.stringify(data)).content;
    this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;
    this.dataSource = new MatTableDataSource(signos);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  eliminar(signos: Signos): void {
    this.signosService.eliminar(signos).subscribe(data => {
      if (data === 1) {
        this.signosService.getLista(0, 100).subscribe(data => {
          this.signosService.signos.next(data);
          this.signosService.mensaje.next("Se elimino correctamente");
        });
      } else {
        this.signosService.mensaje.next("No se pudo eliminar");
      }
    });
  }

  mostrarMas(e) {
    this.signosService.getLista(e.pageIndex, e.pageSize).subscribe(data => {
      this.procesarDatos(data)
    })
  }

}
