import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Paciente } from '../../../_model/paciente';
import { FormGroup, FormControl } from '@angular/forms';
import { PacienteService } from '../../../_service/paciente.service';

@Component({
  selector: 'app-paciente-nuevo',
  templateUrl: './paciente-nuevo.component.html',
  styleUrls: ['./paciente-nuevo.component.css']
})
export class PacienteNuevoComponent implements OnInit {

  paciente: Paciente;
  form: FormGroup;

  constructor(public dialogRef: MatDialogRef<PacienteNuevoComponent>,
    @Inject(MAT_DIALOG_DATA) public data, private pacienteService: PacienteService) {
      this.paciente = new Paciente();

      this.form = new FormGroup({
        'id': new FormControl(0),
        'nombres': new FormControl(''),
        'apellidos': new FormControl(''),
        'dni': new FormControl(''),
        'direccion': new FormControl(''),
        'telefono': new FormControl('')
      })
    }

  ngOnInit() {
  }

  operar() {
    this.paciente.idPaciente = this.form.value['id'];
    this.paciente.nombres = this.form.value['nombres'];
    this.paciente.apellidos = this.form.value['apellidos'];
    this.paciente.dni = this.form.value['dni'];
    this.paciente.direccion = this.form.value['direccion'];
    this.paciente.telefono = this.form.value['telefono'];

      //insert
      this.pacienteService.registrar(this.paciente).subscribe(data => {
        if (data === 1) {
          this.pacienteService.getlistarPaciente(0, 100).subscribe(pacientes => {
            this.pacienteService.pacienteCambio.next(pacientes);
            this.pacienteService.mensaje.next('Se registró');
          });
        } else {
          this.pacienteService.mensaje.next('No se registró');
        }
      });

    this.dialogRef.close(true)
  }

  cerrar() {
    this.dialogRef.close()
  }

}
