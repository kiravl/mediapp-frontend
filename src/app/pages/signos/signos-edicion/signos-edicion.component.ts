import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { SignosService } from '../../../_service/signos.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Signos } from '../../../_model/signos';
import { PacienteService } from '../../../_service/paciente.service';
import { Paciente } from '../../../_model/paciente';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { DatePipe } from '@angular/common';
import { MatDialog } from '@angular/material';
import { PacienteNuevoComponent } from '../paciente-nuevo/paciente-nuevo.component';

@Component({
  selector: 'app-signos-edicion',
  templateUrl: './signos-edicion.component.html',
  styleUrls: ['./signos-edicion.component.css']
})
export class SignosEdicionComponent implements OnInit {

  id: number
  signos: Signos
  form: FormGroup
  edicion: boolean = false
  pacientes: Paciente[] = []
  pacientesFiltrados: Observable<Paciente[]>

  constructor(private signosService: SignosService, private pacienteService: PacienteService, private route: ActivatedRoute, private router: Router, public dialog: MatDialog) {
    this.signos = new Signos()
    this.form = new FormGroup({
      'id': new FormControl(0),
      'fecha': new FormControl(new Date()),
      'temperatura': new FormControl(''),
      'pulso': new FormControl(''),
      'ritmoRespiratorio': new FormControl(''),
      'paciente': new FormControl(null)
    })
    this.pacientesFiltrados = this.form.get('paciente').valueChanges.pipe(startWith(''), map(x => x ? this.filtrarPacientes(x) : this.pacientes.slice()))
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = params['id']
      this.edicion = params['id'] != null
      this.initForm()
    })
    this.pacienteService.getlistar().subscribe(data => {
      this.pacientes = data;
    })
  }

  private filtrarPacientes(nombre: string) {
    return this.pacientes.filter(x => x.nombres.toLocaleLowerCase().indexOf(nombre.toLocaleLowerCase()) === 0)
  }

  private initForm() {
    if (this.edicion) 
      this.signosService.getById(this.id).subscribe(data => {
        this.form = new FormGroup({
          'id': new FormControl(data.id),
          'fecha': new FormControl(new Date(data.fecha)),
          'temperatura': new FormControl(data.temperatura),
          'pulso': new FormControl(data.pulso),
          'ritmoRespiratorio': new FormControl(data.ritmoRespiratorio),
          'paciente': new FormControl(data.paciente)
        })
      })
  }

  operar() {
    this.signos.id = this.form.value['id'];
    this.signos.fecha = (new DatePipe('es-PE')).transform(this.form.value["fecha"], 'yyyy-MM-dd')
    this.signos.temperatura = this.form.value['temperatura'];
    this.signos.pulso = this.form.value['pulso'];
    this.signos.ritmoRespiratorio = this.form.value['ritmoRespiratorio'];
    this.signos.paciente = this.form.value['paciente'];

    if (this.edicion) {
      //update
      this.signosService.modificar(this.signos).subscribe(data => {
        if (data === 1) {
          this.signosService.getLista(0, 100).subscribe(data => {
            this.signosService.signos.next(data);
            this.signosService.mensaje.next('Se modificó');
          })
        } else {
          this.signosService.mensaje.next('No se modificó');
        }
      })
    } else {
      //insert
      this.signosService.registrar(this.signos).subscribe(data => {
        if (data === 1) {
          this.signosService.getLista(0, 100).subscribe(pacientes => {
            this.signosService.signos.next(pacientes);
            this.signosService.mensaje.next('Se registró');
          });
        } else {
          this.signosService.mensaje.next('No se registró');
        }
      })
    }

    this.router.navigate(['signos'])
  }

  displayFn(x: Paciente) {
    if (x == undefined || x == null) return ''
    return `${x.dni} - ${x.nombres} ${x.apellidos}`
  }

  openDialiog() {
    this.dialog.open(PacienteNuevoComponent).afterClosed().subscribe(result => {
      if (result)
        this.pacienteService.getlistar().subscribe(data => {
          this.pacientes = data;
        })
    })
  }

}