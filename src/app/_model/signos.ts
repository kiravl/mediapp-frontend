import { Paciente } from "./paciente";

export class Signos {

    id: number
    fecha: string
    temperatura: string
    pulso: string
    ritmoRespiratorio: string
    paciente: Paciente

}