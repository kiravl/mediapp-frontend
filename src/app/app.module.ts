import { ConsultaService } from './_service/consulta.service';
import { ExamenService } from './_service/examen.service';
import { MedicoService } from './_service/medico.service';
import { GuardService } from './_service/guard.service';
import { LoginService } from './_service/login.service';
import { PacienteService } from './_service/paciente.service';
import { MaterialModule } from './material/material.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { PacienteComponent } from './pages/paciente/paciente.component';
import { PacienteEdicionComponent } from './pages/paciente/paciente-edicion/paciente-edicion.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { EspecialidadService } from './_service/especialidad.service';
import { EspecialidadComponent } from './pages/especialidad/especialidad.component';
import { EspecialidadEdicionComponent } from './pages/especialidad/especialidad-edicion/especialidad-edicion.component';
import { ExamenComponent } from './pages/examen/examen.component';
import { ExamenEdicionComponent } from './pages/examen/examen-edicion/examen-edicion.component';
import { MedicoComponent } from './pages/medico/medico.component';
import { DialogoComponent } from './pages/medico/dialogo/dialogo.component';
import { ConsultaComponent } from './pages/consulta/consulta.component';
import { BuscarComponent } from './pages/consulta/buscar/buscar.component';
import { DialogoDetalleComponent } from './pages/consulta/buscar/dialogo-detalle/dialogo-detalle.component';
import { EspecialComponent } from './pages/consulta/especial/especial.component';
import { Not403Component } from './pages/not403/not403.component';

import { HashLocationStrategy, LocationStrategy, registerLocaleData } from '@angular/common';
import { ReporteComponent } from './pages/reporte/reporte.component';

import { PdfViewerModule } from 'ng2-pdf-viewer';
import { RecuperarComponent } from './login/recuperar/recuperar.component';
import { TokenComponent } from './login/recuperar/token/token.component';
import { PerfilComponent } from './pages/perfil/perfil.component';
import { SignosComponent } from './pages/signos/signos.component';
import { SignosEdicionComponent } from './pages/signos/signos-edicion/signos-edicion.component';
import { MatAutocompleteModule } from '@angular/material';
import { MatDialogModule } from '@angular/material/dialog';
import peru from '@angular/common/locales/es-PE';
import peruExtra from '@angular/common/locales/extra/es-PE';
import { PacienteNuevoComponent } from './pages/signos/paciente-nuevo/paciente-nuevo.component';

registerLocaleData(peru, 'es-PE', peruExtra)

@NgModule({
  declarations: [
    AppComponent,
    PacienteComponent,
    PacienteEdicionComponent,
    LoginComponent,
    EspecialidadComponent,
    EspecialidadEdicionComponent,
    ExamenComponent,
    ExamenEdicionComponent,
    MedicoComponent,
    DialogoComponent,
    ConsultaComponent,
    BuscarComponent,
    DialogoDetalleComponent,
    EspecialComponent,
    Not403Component,
    ReporteComponent,
    RecuperarComponent,
    TokenComponent,
    PerfilComponent,
    SignosComponent,
    SignosEdicionComponent,
    PacienteNuevoComponent
  ],
  entryComponents: [DialogoComponent, DialogoDetalleComponent, PacienteNuevoComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    PdfViewerModule,
    MatAutocompleteModule,
    MatDialogModule
  ],
  providers: [
    PacienteService, LoginService, MedicoService, ExamenService, EspecialidadService, ConsultaService, GuardService,
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    { provide: LOCALE_ID, useValue: 'es-PE' }
  ], bootstrap: [AppComponent]
})
export class AppModule { }
